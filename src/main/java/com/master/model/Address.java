package com.master.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*If use @Data annotation, then it is giving stack overflow error, so used @Setter and @Getter
 Also, If we use @ToSTring in child clases then also it is giving the exception*/
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "address", schema = "spring")
public class Address {

	@Id
	private Integer id;
	@Column
	private String city;
	@Column
	private String dist;

	@ManyToOne
	@JoinColumn(name = "emp_id", nullable = false)
	private Employee emp;

	@Override
	public String toString() {
		return "Address [id=" + id + ", city=" + city + ", dist=" + dist + "]";
	}

}
