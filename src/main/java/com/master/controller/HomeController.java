package com.master.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.master.model.Employee;
import com.master.repository.EmployeeRepo;

@RestController
public class HomeController {

	@Autowired
	EmployeeRepo repo;
	
	
	@GetMapping("/getEmp")
	public String getEmployee()
	{
		Optional<Employee> opt = repo.findById(1);
		return opt.get().toString();
	}
}
